<?php

/*
 *  CONTROLADOR FRONTAL
 */
// web/index.php
session_start();

// carga del modelo y los controladores
// ... modelo
require_once __DIR__ . '/../app/model/Config.php';
require_once __DIR__ . '/../app/model/Departamento.php';
require_once __DIR__ . '/../app/model/IModelo.php';
require_once __DIR__ . '/../app/model/ModeloFichero.php';
require_once __DIR__ . '/../app/model/ModeloMysql.php';
require_once __DIR__ . '/../app/model/Trabajador.php';

// ... controladores
require_once __DIR__ . '/../app/controller/ControladorDepartamento.php';
require_once __DIR__ . '/../app/controller/ControladorTrabajador.php';
require_once __DIR__ . '/../app/controller/ControladorModelo.php';
require_once __DIR__ . '/../app/controller/ControladorSesion.php';

// ... funciones
require_once __DIR__ . '/../app/utils/funciones.php';

// ENRUTAMIENTO
$map = array(
    //mapeo del controlador de sesion
    'iniciaSesion' => array('controller' =>'ControladorSesion', 'action' =>'iniciarSesion'),
    'cierraSesion'  => array('controller' =>'ControladorSesion', 'action' =>'cerrarSesion'),
    'muestraLogin'  => array('controller' =>'ControladorSesion', 'action' =>'mostrarLogin'),
    'irAInicio'  => array('controller' =>'ControladorSesion', 'action' =>'irAInicio'),
    
    //mapeo del controlador de modelo
    'obtenModelo' => array('controller' =>'ControladorModelo', 'action' =>'obtenerModelo'),
    'opcionBBDD'  => array('controller' =>'ControladorModelo', 'action' =>'opcionBBDD'),
    'irAPrincipal'  => array('controller' =>'ControladorModelo', 'action' =>'irAPrincipal'),
    
    //mapeo del controlador de departamento
    'muestraDepartamentos' => array('controller' =>'ControladorDepartamento', 'action' => 'mostrarDepartamentos'),
    'vistaAltaDepartamento' => array('controller' =>'ControladorDepartamento', 'action' => 'altaDepartamento'),
    'guardaDepartamento' => array('controller' =>'ControladorDepartamento', 'action' => 'guardarDepartamento'),
    'borraDepartamento' => array('controller' =>'ControladorDepartamento', 'action' => 'borrarDepartamento'),
    
    //mapeo del controlador de trabajador
    'muestraTrabajadores' => array('controller' =>'ControladorTrabajador', 'action' => 'mostrarTrabajadores'),
    'vistaAltaTrabajador' => array('controller' =>'ControladorTrabajador', 'action' => 'altaTrabajador'),
    'guardaTrabajador' => array('controller' =>'ControladorTrabajador', 'action' => 'guardarTrabajador'),
    'borraTrabajador' => array('controller' =>'ControladorTrabajador', 'action' => 'borrarTrabajador'),

);

// PARSEO DE LA RUTA
if (isset($_GET['ctl'])) {
    if (isset($map[$_GET['ctl']])) {
        $ruta = $_GET['ctl'];
    } else {
        header('Status: 404 Not Found');
        echo '<html><body><h1>Error 404: No existe la ruta <i>' .
        $_GET['ctl'] .
        '</p></body></html>';
        exit;
    }
} else 
    $ruta = 'muestraLogin';

$controlador = $map[$ruta];

// EJECUCIÓN DEL CONTROLADOR ASOCIADO A LA RUTA
if (method_exists($controlador['controller'],$controlador['action'])) 
    call_user_func(array(new $controlador['controller'], $controlador['action']));
else {
    header('Status: 404 Not Found');
    echo '<html><body><h1>Error 404: El controlador <i>' .
    $controlador['controller'] . '->' .$controlador['action'] .
    '</i> no existe</h1></body></html>';
}
?>