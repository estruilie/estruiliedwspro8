<?php
/*include_once '../model/ModeloMysql.php';
include_once '../model/ModeloFichero.php';
include_once '../utils/funciones.php';*/

class ControladorModelo {
    function obtenerModelo() {
        if ( isset($_REQUEST["modelo"]) ) {
            
            $_SESSION['modelo'] = recoge('modelo');
            if ( $_SESSION['modelo'] == 1 )
                //header('Location: ../templates/index2.php');
                require __DIR__ . '/../templates/index2.php';
            else
                //header('Location: ../templates/indexBBDD.php');
                require __DIR__ . '/../templates/indexBBDD.php';
        }
    }

    function opcionBBDD() {
        if ( isset($_REQUEST["instalar"]) ) {

            $_REQUEST["instalar"] = recoge('instalar');
            if ( $_REQUEST["instalar"] == 1 ) {
                $modelo = new ModeloMysql();
                $modelo->instalarBD();
                //header('Location: ../templates/VistaInstalacionBBDD.php');
                require __DIR__ . '/../templates/VistaInstalacionBBDD.php';
            }
            else
                //header('Location: ../templates/index2.php');
                require __DIR__ . '/../templates/index2.php';
        }   
    }
    
    function irAPrincipal() {
        require __DIR__ . '/../templates/index2.php';
    }
}

?>

