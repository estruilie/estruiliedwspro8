<?php

class ControladorTrabajador {
    function mostrarTrabajadores() {
        error_reporting(E_ERROR  | E_PARSE ); //evitamos un aviso NOTICE cuando no hay trabajadores a mostrar
        
        if ( $_SESSION['modelo']==1 )
            $modelo = new ModeloFichero();
        else $modelo = new ModeloMysql();
        
        if ( count($modelo->leerTrabajadores())>0 )
            $params = array('trabajadores' => $modelo->leerTrabajadores(),
                        'modelo' => $modelo);
        else $params = null;
       
        require __DIR__.'/../templates/VistaTrabajadores.php';
    }
    
    function altaTrabajador() {
        require __DIR__.'/../templates/VistaAltaTrabajador.php';
    }
    
    function guardarTrabajador() {
        error_reporting(E_ERROR  | E_PARSE ); //evitamos warnings cuando hay problemas de permisos con los ficheros
        
        if ( isset($_REQUEST["id"]) && isset($_REQUEST["nombre"]) ) {
            $id = recoge("id");
            $nombre = recoge("nombre");
            $departamento = recoge("departamento");

            $trabajador = new Trabajador($id, $nombre, $departamento);

            if ( $_SESSION['modelo']==1 )
                $modelo = new ModeloFichero();
            else
                $modelo = new ModeloMysql();
            $grabado = $modelo->guardarTrabajador($trabajador);

            if ( $grabado === false ) 
                //header("Location: ../templates/VistaOperacionNoOK.php");
                require __DIR__.'/../templates/VistaOperacionNoOK.php';
            else
                //header("Location: ../templates/VistaOperacionOK.php");
                require __DIR__.'/../templates/VistaOperacionOK.php';
        }
    }

    function borrarTrabajador() {
        $id = recoge('id');
        if ( $_SESSION['modelo']==1 )
            $modelo = new ModeloFichero();
        else
            $modelo = new ModeloMysql();
        $modelo->borrarTrabajador($id);

        $ContDep = new ControladorTrabajador();
        $ContDep->mostrarTrabajadores();    
    }

    function nuevoIdTra() {
        error_reporting(E_ERROR  | E_PARSE ); //evitamos un aviso NOTICE cuando no hay todavía ningún trabajador
        if ( $_SESSION['modelo']==1 )
            $modelo = new ModeloFichero();
        else
            $modelo = new ModeloMysql();

        $todos = $modelo->leerTrabajadores();
        if ( count($todos)>0 ) {
            $ultimoIndice = count($todos)-1;
            $ultimaId = $todos[$ultimoIndice]->getId();
            echo $ultimaId+1;
        } else 
            echo "1";
    }

    function rellenarCombo() {
        if ( $_SESSION['modelo']==1 )
            $modelo = new ModeloFichero();
        else
            $modelo = new ModeloMysql();
        $departamentos = $modelo->leerDepartamentos();
        if ( $departamentos ) {
            foreach ($departamentos as $d) {
                echo "<option value='" . $d->getId()."'>" . $d->getNombre() . "</option>";
            }
        }
        else
            echo "<option value=''>Todavía no existe ningún departamento</option>";
    }
}
?>





