<?php
/*if (strlen(session_id()) < 1)
    session_start();
error_reporting(E_ERROR  | E_PARSE | E_NOTICE);
//ini_set('display_errors','1');

require_once "../utils/funciones.php";
include_once '../model/ModeloMysql.php';
include_once '../model/ModeloFichero.php';*/

class ControladorDepartamento {
    function mostrarDepartamentos() {
        error_reporting(E_ERROR  | E_PARSE ); //evitamos un aviso NOTICE cuando no hay departamentos a mostrar
        
        if ( $_SESSION['modelo']==1 )
            $modelo = new ModeloFichero();
        else 
            $modelo = new ModeloMysql();
        
        $params = array('departamentos' => $modelo->leerDepartamentos());
        
        require __DIR__.'/../templates/VistaDepartamentos.php';
    }

    function altaDepartamento() {
        require __DIR__.'/../templates/VistaAltaDepartamento.php';
    }
    
    function guardarDepartamento() {
        error_reporting(E_ERROR  | E_PARSE ); //evitamos warnings cuando hay problemas de permisos con los ficheros
        
        if ( isset($_REQUEST["id"]) && isset($_REQUEST["nombre"]) && isset($_REQUEST["ubicacion"]) ) {
            $id = recoge("id");
            $nombre = recoge("nombre");
            $ubicacion = recoge("ubicacion");

            $departamento = new Departamento($id, $nombre, $ubicacion);

            if ( $_SESSION['modelo']==1 )
                $modelo = new ModeloFichero();
            else 
                $modelo = new ModeloMysql();
            $grabado = $modelo->guardarDepartamento($departamento);

            if ( $grabado === false ) 
                //header("Location: ../templates/VistaOperacionNoOK.php");
                require __DIR__.'/../templates/VistaOperacionNoOK.php';
            else
                //header("Location: ../templates/VistaOperacionOK.php");
                require __DIR__.'/../templates/VistaOperacionOK.php';
        }
    }

    function nuevoIdDep() {
        error_reporting(E_ERROR  | E_PARSE ); //evitamos un aviso NOTICE cuando no hay todavía ningún departamento
        if ( $_SESSION['modelo']==1 )
            $modelo = new ModeloFichero();
        else
            $modelo = new ModeloMysql();
        $todos = $modelo->leerDepartamentos();
        if ( count($todos)>0 ) {
            $ultimoIndice = count($todos)-1;
            $ultimaId = $todos[$ultimoIndice]->getId();
            echo $ultimaId+1;
        } else 
            echo "1";
    }

    function borrarDepartamento() {
        $id = recoge('id');
        if ( $_SESSION['modelo']==1 )
            $modelo = new ModeloFichero();
        else
            $modelo = new ModeloMysql();
        $modelo->borrarDepartamento($id);
        
        $ContDep = new ControladorDepartamento();
        $ContDep->mostrarDepartamentos();    
    }
}
?>    