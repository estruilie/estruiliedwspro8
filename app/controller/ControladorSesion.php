<?php
//include_once "../utils/funciones.php";

class ControladorSesion {
    function iniciarSesion() {
        if ( isset($_REQUEST["nombre"]) ) {
            if ( !isset($_SESSION) ) {
                session_start();
            }

            $nombre = recoge("nombre");
            $password = recoge("password");

            if ( $nombre == "alumno" && $password == "alumno" ) {
                $_SESSION["logueado"] = "logueado";
                //header("Location: ../app/templates/index.php");
                require __DIR__ . '/../templates/index.php';
            } else 
                //header("Location: ../app/templates/login.php");
                require __DIR__ . '/../templates/login.php';
        }
    }

    function cerrarSesion() {
        if( !isset($_SESSION) ) session_start();  
        unset($_SESSION["logueado"]);

        session_destroy();

        header("Location: ../web/index.php");
    }
    
    function mostrarLogin() {
        require __DIR__ . '/../templates/login.php';
    }
    
    function irAInicio() {
        require __DIR__ . '/../templates/index.php';
    }
}

?>