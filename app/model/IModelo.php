<?php

interface Modelo {
    public function leerDepartamentos();
    public function guardarDepartamento($departamento);
    public function leerTrabajadores();
    public function guardarTrabajador($trabajador);
    public function nombreDepartamentoPorId($id);
    public function instalarBD();
}