<?php

/* 
 * ESTA CLASE PERMITIRÁ EXTRAER LA INFORMACIÓN NECESARIA DE LAS CLASES DEPARTAMENTO Y TRABAJADOR CON EL MODELO FICHERO
 */

class ModeloMysql implements Modelo {
    
    
    // métodos
    function leerDepartamentos() {
        try {
        
            $db = $this->conectaDB();
            $sql = "SELECT * FROM Departamento";
            $result = $db->prepare($sql);
            $result->execute();

            foreach ($result as $valor) {
                $d = new Departamento($valor['id'], $valor['nombre'], $valor['ubicacion']);
                $departamentos[] = $d;
            }

            $db = null;
            
            return $departamentos;
            
        } catch (Exception $e) {
            echo $sql . "<br>" . $e->getMessage();
            $db = null;
        }
        
    }
     
    function guardarDepartamento($departamento) {
        try {
            $dId = $departamento->getId();
            $dNo = $departamento->getNombre();
            $dUb = $departamento->getUbicacion();

            $db = $this->conectaDB();
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = "INSERT INTO Departamento(id,nombre,ubicacion) VALUES (:dId,:dNo,:dUb)";
            $result = $db->prepare($sql);
            $result->execute(array(":dId"=>$dId,":dNo"=>$dNo,":dUb"=>$dUb));
            
        } catch (Exception $e) {
            echo $sql . "<br>" . $e->getMessage();
        }
        $db = null;
        
    }
    
    function borrarDepartamento($id) {
    
            $db = $this->conectaDB();
            
            $sql = "DELETE FROM Departamento WHERE id=:dId;";
            $result = $db->prepare($sql);
            $result->execute(array(":dId"=>$id));
            
            $db = null;
    }
    
    function leerTrabajadores() {
        try {
        
            $db = $this->conectaDB();
            $sql = "SELECT * FROM Trabajador";
            $result = $db->prepare($sql);
            $result->execute();

            foreach ($result as $valor) {
                $t = new Trabajador($valor[0], $valor[1], $valor[2]);
                $trabajadores[] = $t;
            }

            $db = null;

            return $trabajadores;
            
        } catch (Exception $e) {
            echo $sql . "<br>" . $e->getMessage();
            $db = null;
        }
    
    }
    
    function guardarTrabajador($trabajador) {
        
        try {
            $tId = $trabajador->getId();
            $tNo = $trabajador->getNombre();
            $tDe = $trabajador->getDepartamento();

            $db = $this->conectaDB();
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = "INSERT INTO Trabajador(id,nombre,departamento) VALUES (?,?,?)";
            $result = $db->prepare($sql);
            $result->execute(array($tId,$tNo,$tDe));
            
        } catch (Exception $e) {
            echo $sql . "<br>" . $e->getMessage();
        }
        $db = null;
        
    }
    
    function borrarTrabajador($id) {
        $db = $this->conectaDB();
            
            $sql = "DELETE FROM Trabajador WHERE id=:dId;";
            $result = $db->prepare($sql);
            $result->execute(array(":dId"=>$id));
            
            $db = null;
    }
    
    function nombreDepartamentoPorId($id) {  //$id es una REFERENCIA al atributo departamento de un objeto trabajador -> NO PODEMOS COMPARAR REFERENCIAS
        if ( $_SESSION['modelo']==1 )
            $modelo = new ModeloFichero();
        else $modelo = new ModeloMysql();
        $todos = $modelo->leerDepartamentos();
        
        /*
            Anatomy of a serialize() value:

            String
            s:size:value;

        */
        // Debemos obtener value del atributo departamento del trabajador y 
        // comparar con el value del atributo id del departamento
        $id_tra = serialize($id);  // Obtenemos algo así como--> s:2:"1 ";

        // Extraemos el primer carácter de value del trabajador
        $array_id_tra = explode('"', $id_tra);
        $idTra = substr($array_id_tra[1], 0, 1);

       // $idd = serialize($todos[$id-1]->getId());
        //echo " idd=".$idd."<br/>";

        foreach ($todos as $d) {

            $id_dep = serialize($d->getId());  // Obtenemos algo así como--> s:1:"1";
            $array_id_dep = explode('"', $id_dep);
            $idDep = $array_id_dep[1];

            if (  $idTra == $idDep ) {
                return $d->getNombre();
                break;
            }
        }
    }
    
    function instalarBD() {
        try {
            $this->creaDB();
            $db = $this->conectaDB();
            $this->creaTablas($db);
            $db = null;
            
        } catch (PDOException $e) {            
            require __DIR__ . '/../templates/indexBBDD.php';
            echo "¡Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        
    }
    
    // Función para conectar con la BBDD
    function conectaDB() {
        
        try {
            $miConexion = "mysql:host=".Config::$bdhostname.";dbname=".Config::$bdnombre;
            $db = new PDO($miConexion, Config::$bdusuario, Config::$bdclave);
            $db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);
            
            return($db);
        } catch (PDOException $e) {
            require __DIR__ . '/../templates/indexBBDD.php';
            print "<p>Error: No puede conectarse con la base de datos.</p>\n";
            echo "¡Error!: " . $e->getMessage() . "<br/>";
            exit();
        }
    }
    
    // Función que crea la BBDD
    function creaDB() {
        $hostname = Config::$bdhostname;
        try {
            $db = new PDO("mysql:host=$hostname", "root", "");
            $db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);
            
            $consulta = "CREATE DATABASE ".Config::$bdnombre;  // CREAMOS LA BBDD
            $db->query($consulta);
            
            
            $consulta = "CREATE USER ".Config::$bdusuario;      // CREAMOS EL USUARIO
            $db->query($consulta);
            
            
            $consulta = "GRANT ALL ON ".Config::$bdnombre.".* TO ".
                    Config::$bdusuario."@".Config::$bdhostname."IDENTIFIED BY".Config::$bdclave;      // ASIGNAMOS PASSWORD PRIVILEGIOS AL USUARIO
            $db->query($consulta);
            
            $db = NULL;
            
        } catch (PDOException $e) {
            //cabecera("Error grave");
            print "<p>Error: No puede conectarse con la base de datos.</p>\n";
            
            print "<p>Error: " . $e->getMessage() . "</p>\n";
            //pie();
            exit();
        }
    }
    
    // Función que crea las tablas Departamento y Trabajador
    function creaTablas($db) {
                
        $sql = "CREATE TABLE Departamento (
            id INTEGER UNSIGNED NOT NULL,
            nombre VARCHAR(50),
            ubicacion VARCHAR(50),
            PRIMARY KEY(id)
            )";
        
        $db->query($sql);
        
        // inserción de valores para muestra de la aplicación
        
        $sql = "INSERT INTO Departamento (id, nombre, ubicacion) VALUES (1, 'Direccion', 'Serpis, 18 - Valencia');"
                . "INSERT INTO Departamento (id, nombre, ubicacion) VALUES (2, 'Recursos humanos', 'Serpis, 18 - Valencia');"
                . "INSERT INTO Departamento (id, nombre, ubicacion) VALUES (3, 'Comerciales', 'Colon, 2 - Valencia');";
        
        $db->query($sql);
        
        $sql = "CREATE TABLE Trabajador (
            id INTEGER UNSIGNED NOT NULL,
            nombre VARCHAR(50),
            departamento INTEGER UNSIGNED NOT NULL,
            PRIMARY KEY(id),
            FOREIGN KEY (departamento) REFERENCES Departamento(id)
            )";
        
        $db->query($sql);
        
         // inserción de valores para muestra de la aplicación
        
        $sql = "INSERT INTO Trabajador (id, nombre, departamento) VALUES (1, 'Antonio Fernandez', 1);"
                . "INSERT INTO Trabajador (id, nombre, departamento) VALUES (2, 'Ana Garcia', 2);"
                . "INSERT INTO Trabajador (id, nombre, departamento) VALUES (3, 'Isabel Martinez', 3);"
                . "INSERT INTO Trabajador (id, nombre, departamento) VALUES (4, 'Jose Luis Ramiro', 3);";
        
        $db->query($sql);
        
        $db = NULL;
    }
}