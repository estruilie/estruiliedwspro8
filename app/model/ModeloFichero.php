<?php

/* 
 * ESTA CLASE PERMITIRÁ EXTRAER LA INFORMACIÓN NECESARIA DE LAS CLASES DEPARTAMENTO Y TRABAJADOR CON EL MODELO FICHERO
 */

class ModeloFichero implements Modelo {
    
    // atributos
    private $ficheroDep = "../app/txt/departamentos.txt"; 
    private $ficheroTra = "../app/txt/trabajadores.txt";
    
    // métodos
    function leerDepartamentos() {

        if ( !is_file($this->ficheroDep) ) {  
            return;
        }
        
        $arrayDepartamentos = file($this->ficheroDep);
        
        foreach ($arrayDepartamentos as $departamento) { //extraemos cada línea del fichero
            $dep = explode(";",$departamento);  // generamos un array de strings separados por ,
            $d = new Departamento($dep[0],$dep[1],$dep[2]); // creamos el objeto departamento
            $departamentos[] = $d; // lo añadimos al array de departamentos
        }
        
        if ( count($arrayDepartamentos)>0 )
            return $departamentos;
        else return $departamentos = null;
        
    }
     
    function guardarDepartamento($departamento) {
        
        $fich = fopen($this->ficheroDep, "a");
        $grabado = fwrite($fich, $departamento->getId() . ';' . $departamento->getNombre() . ';' . $departamento->getUbicacion() . PHP_EOL);
        fclose($fich);
        return $grabado;
        
    }
    
    function borrarDepartamento($id) {
    
        $fich = fopen("../app/txt/departamentos.tmp", "w+");
        $arrayDepartamentos = file($this->ficheroDep);
        
        foreach ($arrayDepartamentos as $departamento) { //extraemos cada línea del fichero
            $dep = explode(";",$departamento);  // generamos un array de strings separados por ,
            if ( $id != $dep[0] )
                $grabado = fwrite($fich, $departamento);           
        }
        
        fclose ($fich);
        unlink($this->ficheroDep);
        rename("../app/txt/departamentos.tmp", "../app/txt/departamentos.txt");
    }
       
    function leerTrabajadores() {
       
        if ( !is_file($this->ficheroTra) ) {  
            return;
        }
        
        $arrayTrabajadores = file($this->ficheroTra);
        
        foreach ($arrayTrabajadores as $trabajador) { //extraemos cada línea del fichero
            $tra = explode(";",$trabajador);  // generamos un array de strings separados por ,
            $t = new Trabajador($tra[0],$tra[1],$tra[2]); // creamos el objeto trabajador
            $trabajadores[] = $t; // lo añadimos al array de trabajadores
        }
        
        if ( count($arrayTrabajadores)>0 )
            return $trabajadores;
        else return $trabajadores = null;
        
    }
    
    function guardarTrabajador($trabajador) {
        
        $fich = fopen($this->ficheroTra, "a");
        $grabado = fwrite($fich, $trabajador->getId() . ';' . $trabajador->getNombre() .  ';' .$trabajador->getDepartamento() . PHP_EOL);
        fclose($fich);
        return $grabado;
        
    }
    
    function borrarTrabajador($id) {
    
        $fich = fopen("../app/txt/trabajadores.tmp", "w+");
        $arrayTrabajadores = file($this->ficheroTra);
        
        foreach ($arrayTrabajadores as $trabajador) { //extraemos cada línea del fichero
            $tra = explode(";",$trabajador);  // generamos un array de strings separados por ,
            if ( $id != $tra[0] )
                $grabado = fwrite($fich, $trabajador);           
        }
        
        fclose ($fich);
        unlink($this->ficheroTra);
        rename("../app/txt/trabajadores.tmp", "../app/txt/trabajadores.txt");
    }
    
    function nombreDepartamentoPorId($id) {  //$id es una REFERENCIA al atributo departamento de un objeto trabajador -> NO PODEMOS COMPARAR REFERENCIAS
        if ( $_SESSION['modelo']==1 )
            $modelo = new ModeloFichero();
        $todos = $modelo->leerDepartamentos();
        
        /*
            Anatomy of a serialize() value:

            String
            s:size:value;

        */
        // Debemos obtener value del atributo departamento del trabajador y 
        // comparar con el value del atributo id del departamento
        $id_tra = serialize($id);  // Obtenemos algo así como--> s:2:"1 ";

        // Extraemos el primer carácter de value del trabajador
        $array_id_tra = explode('"', $id_tra);
        $idTra = substr($array_id_tra[1], 0, 1);

       // $idd = serialize($todos[$id-1]->getId());
        //echo " idd=".$idd."<br/>";

        foreach ($todos as $d) {

            $id_dep = serialize($d->getId());  // Obtenemos algo así como--> s:1:"1";
            $array_id_dep = explode('"', $id_dep);
            $idDep = $array_id_dep[1];

            if (  $idTra == $idDep ) {
                return $d->getNombre();
                break;
            }
        }
    }
    
    function instalarBD() {
        //no requerido
    }
}