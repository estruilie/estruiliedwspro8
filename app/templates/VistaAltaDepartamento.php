<?php ob_start() ?>
        
        <h2>Introduzca los datos del nuevo departamento</h2>
        <form method="POST" action="index.php?ctl=guardaDepartamento" >
            
            <label for="id">Id:</label>
            <input size="4" type="text" name="id" readonly value="<?php
                    $controllerDep = new ControladorDepartamento();
                    echo $controllerDep->nuevoIdDep();
                ?>"/>
            <br/><br/>
            
            <label for="nombre">Nombre:</label>
            <input size="50" type="text" name="nombre" required placeholder="Nombre del departamento" title="Por favor, introduce un nombre correcto. Sólo letras y espacios" pattern="[a-zA-Z\W]+" />
            <br/><br/>
            
            <label for="ubicacion">Ubicación:</label>
            <input size="50" type="text" name="ubicacion" required placeholder="Valencia" title="Por favor, introduce un nombre correcto. Sólo letras, números, espacios y comas" pattern="[a-zA-Z0-9,\W]+" />
            <br/><br/><br/><hr/><br/><br/>
            
            <input type="submit" name="Enviar" value="Enviar" />
            <input type="reset" name="Borrar" value="Borrar" />
            <br/><br/>

        </form>
        <br/><h2><a href="index.php?ctl=irAPrincipal"> Volver a Departamentos & Trabajadores </a></h2>
        
<?php
$contenido = ob_get_clean();
$titulo = "Alta de departamento";
include "layout.php";
?>