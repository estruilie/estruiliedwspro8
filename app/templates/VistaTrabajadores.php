<?php ob_start() ?>
   
<div class="tablas">
    <h2> TRABAJADORES EXISTENTES: </h2>

    <table border="2">
    <tr>
    <th>ID Trabajador</th>
    <th>Nombre Trabajador</th>
    <th>Departamento Asociado</th>
    <th>Borrar</th>
    </tr>

    <?php 
        if ( count($params['trabajadores']) !== 0 ) :   
            foreach ($params['trabajadores'] as $trabajador) : ?>
                <tr>
                    <td><?php echo $trabajador->getId() ?></td>
                    <td><?php echo $trabajador->getNombre() ?></td>
                    <td><?php echo $modelo->nombreDepartamentoPorId($trabajador->getDepartamento()) ?></td>
                    <td><a href="index.php?ctl=borraTrabajador&id=<?php echo $trabajador->getId() ?>">Borrar</a></td>
                </tr>
            <?php endforeach;    /*"<?php echo $departamento->getId() ?>"*/
        endif; ?>   

    </table>
    <br/>


</div>
<br/><h2><a href="index.php?ctl=vistaAltaTrabajador"> Dar de alta un nuevo trabajador </a></h2>
<br/><h2><a href="index.php?ctl=irAPrincipal"> Volver a Departamentos & Trabajadores </a></h2>
        
<?php
$contenido = ob_get_clean();
$titulo = "Trabajadores";
include "layout.php";
?>