<?php
error_reporting(E_ERROR  | E_PARSE | E_NOTICE);
/* 
 * PIE DE PÁGINA INCLUÍDO EN TODAS LAS PÁGINAS
 */

/*function inicioModelo() {
    // se utiliza para redirigir a la página de inicio del modelo en caso de encontrarse en una distinta

    // $_SERVER['PHP_SELF'] devuelve ruta actual. Pej: /dwst5p1/templates/index.php
    $ini = strpos($_SERVER['PHP_SELF'],"index"); //devuelve la posición de la cadena buscada. False si no encontrada

    // redirigimos a la página de inicio del modelo correspondiente
    if ( $ini == false) {
        if ( $_SESSION['modelo'] == 1 )
            echo "<a href = 'index2.php'> &gt;&gt; Inicio ficheros</a>";
        else
            echo "<a href = 'index2.php'> &gt;&gt; Inicio BBDD</a>";
    }
}*/

function inicioAplicacion() {
    // se utiliza para redirigir a la página de inicio de la aplicación en caso de encontrarse en una distinta

    if( isset($_SESSION["logueado"]) ) {
        if ( $_SESSION["logueado"]=="logueado" ) {
           
            // $_SERVER['QUERY_STRING'] devuelve la query solicitada, por ej: ctl=iniciaSesion
             //devuelve la posición de la cadena buscada. False si no encontrada

            // redirigimos a la página de inicio de la aplicación
            if ( !preg_match("/ctl=iniciaSesion/", $_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING']!="") 
                echo "<a href = 'index.php?ctl=irAInicio'> &gt;&gt; Cambiar modelo</a>";
        }
    }
        
    
}

function linkCerrarSesion() {
    // se utiliza para mostrar un enlace que permita cerrar la sesión

    if ( preg_match("/ctl=/", $_SERVER['QUERY_STRING']) )
        echo "<a href = 'index.php?ctl=cierraSesion'> &gt;&gt; Cerrar sesión</a>";    
} 

echo "<div class='pie1'>";
echo "<p>" . Config::$autor . " - " . Config::$curso . "</p>";
echo "<p>" . Config::$empresa . " - " . Config::$fecha . " - <a href='../docs/Documentacion.pdf' target='_blank' >Ver Documentacion.pdf</a></p>";
echo "</div>";
echo "<div class='pie2'>";
    inicioAplicacion();
echo "</div>";
echo "<div class='pie3'>";
    linkCerrarSesion();
echo "</div>";
?>