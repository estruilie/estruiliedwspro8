<?php ob_start() ?>
   
<div class="tablas">
    <h2> DEPARTAMENTOS EXISTENTES: </h2>
    <table border="2">
        <tr>
        <th>ID Departamento</th>
        <th>Nombre</th>
        <th>Ubicación</th>
        <th>Borrar</th>
        </tr>

        <?php 
        if ( count($params['departamentos']) !== 0 ) :   
            foreach ($params['departamentos'] as $departamento) : ?>
                <tr>
                    <td><?php echo $departamento->getId() ?></td>
                    <td><?php echo $departamento->getNombre() ?></td>
                    <td><?php echo $departamento->getUbicacion() ?></td>
                    <td><a href="index.php?ctl=borraDepartamento&id=<?php echo $departamento->getId() ?>">Borrar</a></td>
                </tr>
            <?php endforeach;   
        endif; ?>
    
    </table>
    <br/>

</div>
<br/><h2><a href="index.php?ctl=vistaAltaDepartamento"> Dar de alta un nuevo departamento </a></h2>
<br/><h2><a href="index.php?ctl=irAPrincipal"> Volver a Departamentos & Trabajadores </a></h2>
        
<?php
$contenido = ob_get_clean();
$titulo = "Departamentos";
include "layout.php";
?>