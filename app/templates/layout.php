<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $titulo?></title>
        <link rel="stylesheet" href="css/index.css">
        <link rel="stylesheet" href="css/estilos.css">
    </head>
    <body>
        <?php include "header.php"; ?>

        <div id="contenido">
            <?php echo $contenido ?>
        </div>

        <?php include "footer.php"; ?>
    </body>
</html>