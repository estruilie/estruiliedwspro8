<?php ob_start() ?>
        
        <h2>Introduzca los datos del nuevo trabajador</h2>
        <form method="POST"  action="index.php?ctl=guardaTrabajador" >
            
            <label for="id">Identificador:</label>
            <input size="4" type="text" name="id" readonly value="<?php
                    $controllerTra = new ControladorTrabajador();
                    $controllerTra->nuevoIdTra();
                ?>"/>
            <br/><br/>

            <label for="nombre">Nombre:</label>
            <input size="50" type="text" name="nombre" required placeholder="Nombre y apellidos" title="Por favor, introduce un nombre correcto. Sólo letras, espacios y comas" pattern="[a-zA-Z,\W]+" />
            <br/><br/>
            
            <label for="departamento">Departamento:</label>
            <select name="departamento">
                <?php 
                    $controllerTrab = new ControladorTrabajador;
                    $controllerTrab->rellenarCombo();?>
            </select>
            <br/><br/><br/><hr/><br/><br/>
            
            <input type="submit" name="Enviar" value="Enviar" />
            <input type="reset" name="Borrar" value="Borrar" />
            <br/><br/>

        </form>
        <br/><h2><a href="index.php?ctl=irAPrincipal"> Volver a Departamentos & Trabajadores </a></h2>
        
<?php
$contenido = ob_get_clean();
$titulo = "Alta de trabajador";
include "layout.php";
?>